import base64
from typing import Optional
import hmac
import hashlib
import json

from fastapi import FastAPI, Form, Cookie
from fastapi.datastructures import Default
from fastapi.responses import Response


app = FastAPI()

SECRET_KEY = "0811a53c5386576a5d6cff2b616a04e13aedfb889e3aad4404efb5a2ba9b2bdb"
PASSWORD_SALT = "351f7a4153677f924f9f106b2d46a43c1cdb4d8c7c8c39436e456e378498274a"


def sign_data(data: str) -> str:
    """
    Возвращает подписанные данные
    """
    return hmac.new(SECRET_KEY.encode(), msg=data.encode(), digestmod=hashlib.sha256).hexdigest().upper()


def get_username_from_signed_string(username_signed: str) -> Optional[str]:
    username_base64, sign = username_signed.split(".")
    username = base64.b64decode(username_base64.encode()).decode()
    valid_sign = sign_data(username)
    if hmac.compare_digest(valid_sign, sign):
        return username


def verify_password(username: str, password: str) -> bool:
    password_hash = hashlib.sha256(
        (password + PASSWORD_SALT).encode()).hexdigest().lower()
    stored_password_hash = users[username]["password"].lower()
    return password_hash == stored_password_hash


users = {
    "admin@admin.ru": {
        "name": "admin",
        # "password": hashlib.sha256(("1234" + PASSWORD_SALT).encode()).hexdigest()
        "password": "5de5baed61596dfbb3f7cdb9609a839c8eb757d5628544ba9d786ad0040183ab",
        "balance": 100000
    },
    "petr@admin.ru": {
        "name": "Petr",
        # "password": hashlib.sha256(("qwer" + PASSWORD_SALT).encode()).hexdigest()
        "password": "d151ab21d3fa135247a07fb61fbb9f05853eee59a7672e85ef9d12ddb1d73651",
        "balance": 200000
    }
}


@app.get("/")
def index_page(username: Optional[str] = Cookie(default=None)):
    # username: Optional[str] = Cookie(default=None) –– если занчение cookie не установленно,
    # то username будет равняться None

    with open("templates/login.html", "r") as f:
        login_page = f.read()

    if not username:
        return Response(login_page, media_type="text/html")

    valid_username = get_username_from_signed_string(username)
    if not valid_username:
        response = Response(login_page, media_type="text/html")
        response.delete_cookie(key="username")
        print("1")
        return response

    try:
        user = users[valid_username]
    except KeyError:
        response = Response(login_page, media_type="text/html")
        response.delete_cookie(key="username")
        print("2")
        return response

    return Response(
        f"Привет, {users[valid_username]['name']}<br />"
        f"Баланс: {users[valid_username]['balance']}",
        media_type="text/html")


@app.post("/login")
def process_login_page(username: str = Form(...), password: str = Form(...)):

    user = users.get(username)
    # Усли username нет в users, то users[username] выдаст ошибку KeyError,
    # а users.get(username) вернет пустое значение.

    if not user or not verify_password(username, password):
        return Response(
            json.dumps({
                "success": False,
                "message": "Я вас не знаю!"
            }),
            media_type="application/json")

    response = Response(
        json.dumps({
            "success": True,
            "message": f"Привет, {user['name']}<br /> Баланс {user['balance']}"
        }),
        media_type="application/json")

    username_signed = base64.b64encode(
        username.encode()).decode() + "." + sign_data(username)

    response.set_cookie(key="username", value=username_signed)
    return response
